module.exports = {
  siteMetadata: {
    title: "Río DJ - DJ Toluca",
    titleTemplate: "%s · Río DJ",
    description:
      " DJ en toluca y metepec. DJ para eventos. Cabinas Iluminadas. Somos Expertos en Musica. Servicios: Pantallas Gigantes, Audio Profesional, Shows Audiovisuales, Zanqueros, Animadoras, Pirotecnia. ",
    siteUrl: "https://riodj.com.mx", // No trailing slash allowed!
    url: "https://riodj.com.mx", // No trailing slash allowed!
    canonical: "https://riodj.com.mx", // No trailing slash allowed!
    image: "/images/snape.jpg", // Path to your image you placed in the 'static' folder
    twitterUsername: "@riodj",
    author: "Kristofer Ortiz",
    canciones: [
      { src: "canciones/Danza_kuduro.mp3", titulo: "Danza Kuduro" },
      { src: "canciones/chachas.mp3", titulo: "Chachas" },
      { src: "canciones/yo-no-se-manana.mp3", titulo: "Yo no sé mañana" },
      { src: "canciones/bidi-bom.mp3", titulo: "Bidi Bom" },
      { src: "canciones/En-su-lugar.mp3", titulo: "En su lugar" },
      { src: "canciones/Remolino.mp3", titulo: "Remolino" },
      { src: "canciones/YMCA.mp3", titulo: "YMCA" },
      {
        src: "canciones/coolgado-en-tus-manos.mp3",
        titulo: "Colgado en tus manos",
      },
      { src: "canciones/1_Colegiala_en_vivo.mp3", titulo: "Colegiala" },
      { src: "canciones/2_Que_Bello_en_vivo.mp3", titulo: "Que Bello" },
      { src: "canciones/3_Balada_Boa_en_vivo.mp3", titulo: "Balada Boa" },
      {
        src: "canciones/4_Levantando_las_manos_en_vivo.mp3",
        titulo: "Levantando las manos",
      },
      { src: "canciones/5_Bruno_Mars_en_vivo.mp3", titulo: "Bruno Mars" },
      { src: "canciones/6_Talento_de_Tv_en_vivo.mp3", titulo: "Talento de Tv" },
      { src: "canciones/7_Cha_Chas.mp3", titulo: "Cha Chas" },
      { src: "canciones/8_Vasos_Vacios.mp3", titulo: "Vasos Vacios" },
    ],
    paquetes: [
      {
        nombre: "DJ Lite",
        feats: [
          "Equipo de audio profesional de ultima generación.",
          "2 Cabezas Robóticas.",
          "4 paneles LED Full color.",
          "2 pantallas 55”",
          "Maquina de niebla",
          "Microfonía inalámbrica Shure para anfitriones.",
          "Dj profesional.",
          "Controlador Pioneer.",
          "Cabina Iluminada por LEDs.",
          "6 horas de servicio.",
          "Paquete de Souvenirs básicos. (6 diferentes tipos).",
        ],
      },
      {
        nombre: "DJ Estándar",
        feats: [
          "Equipo de audio profesional de ultima generación.",
          "2 Cabezas Robóticas.",
          "4 paneles LED. 2 en 1 (estrobo y wash)",
          "2 Pantallas 55”",
          "1 Pantalla de proyección de 120”",
          "Maquina de niebla",
          "Microfonía inalábrica Shure.",
          "Dj profesional.",
          "Tornamesa profesional Pioneer.",
          "Cabina Iluminada por leds.",
          "1 cabezón y 1 zanquero.",
          "6 horas de servicio.",
          "Paquete de Souvenirs básicos. (8 diferentes tipos).",
        ],
      },
      {
        nombre: "DJ Plus",
        feats: [
          "Equipo de audio profesional de ultima generación.",
          "2 Cabezas Robóticas.",
          "4 paneles LED. 2 en 1 (estrobo y wash)",
          "2 Pantallas 55”",
          "2 Pantalla de proyección de 120”",
          "Maquina de niebla",
          "Microfonía inalábrica Shure.",
          "Dj profesional.",
          "Tornamesa profesional Pioneer.",
          "Cabina Iluminada por leds.",
          "2 cabezones y 2 zanqueros.",
          "Robot LED",
          "6 horas de servicio.",
          "Paquete de Souvenirs Plus (15 diferentes tipos).",
          "Sombreros Espuma para festejad@s.",
          "Edición de video semblanza.",
        ],
      },
    ],
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-anchor-links`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/favicon_grupo_rio.svg`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-material-ui`,
      options: {
        stylesProvider: {
          injectFirst: true,
        },
      },
    },
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
        // Add any options here
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // replace "UA-XXXXXXXXX-X" with your own Tracking ID
        trackingId: " UA-38889080-1",
      },
    },
    {
      resolve: "gatsby-plugin-google-tagmanager",
      options: {
        id: "GTM-TMCWCJS",

        // Include GTM in development.
        //
        // Defaults to false meaning GTM will only be loaded in production.
        includeInDevelopment: false,

        // datalayer to be set before GTM is loaded
        // should be an object or a function that is executed in the browser
        //
        // Defaults to null
        defaultDataLayer: { platform: "gatsby" },

        // Specify optional GTM environment details.
        // gtmAuth: "YOUR_GOOGLE_TAGMANAGER_ENVIRONMENT_AUTH_STRING",
        // gtmPreview: "YOUR_GOOGLE_TAGMANAGER_ENVIRONMENT_PREVIEW_NAME",
        // dataLayerName: "YOUR_DATA_LAYER_NAME",

        // Name of the event that is triggered
        // on every Gatsby route change.
        //
        // Defaults to gatsby-route-change
        // routeChangeEventName: "YOUR_ROUTE_CHANGE_EVENT_NAME",
      },
    },
    {
      resolve: `gatsby-source-wordpress-experimental`,
      options: {
        url: "https://riopopblog.com/graphql",
      },
    },
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        output: `/sitemap.xml`,
        // Exclude specific pages or groups of pages using glob parameters
        // See: https://github.com/isaacs/minimatch
        // The example below will exclude the single `path/to/page` and all routes beginning with `category`
        query: `
        {
          site {
            siteMetadata {
              siteUrl
            }
          }

          allSitePage {
            nodes {
              path
            }
          }
        }`,
        createLinkInHead: true,
        resolveSiteUrl: ({ site, allSitePage }) => {
          //Alternatively, you may also pass in an environment variable (or any location) at the beginning of your `gatsby-config.js`.
          return site.siteMetadata.siteUrl
        },
        serialize: ({ site, allSitePage }) =>
          allSitePage.nodes.map(node => {
            return {
              url: `${site.siteMetadata.siteUrl}${node.path}`,
              changefreq: `weekly`,
              priority: 0.5,
            }
          }),
      },
    },
    {
      resolve: "gatsby-plugin-robots-txt",
    },
  ],
}
