const path = require("path")
const { createFilePath } = require("gatsby-source-filesystem")
/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNode, createNodeField } = actions
  if (node.internal.type === "MarkdownRemark") {
    const slug = createFilePath({ node, getNode, basePath: "posts" })
    createNodeField({
      node,
      name: "slug",
      value: slug,
    })
  }
  // Transform the new node here and create a new node or
  // create a new node field.
}

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  const blogPostTemplate = path.resolve(`src/layouts/Post.layout.js`)
  // Query for markdown nodes to use in creating pages.
  // You can query for whatever data you want to create pages for e.g.
  // products, portfolio items, landing pages, etc.
  // Variables can be added as the second function parameter
  return graphql(
    `
      {
        allWpPost {
          nodes {
            slug
          }
        }
      }
    `
  ).then(result => {
    console.log(JSON.stringify(result))
    if (result.errors) {
      throw result.errors
    }

    // Create blog post pages.
    result.data.allWpPost.nodes.forEach(node => {
      createPage({
        // Path for this page — required
        path: `blog/${node.slug}`,
        component: blogPostTemplate,
        context: {
          slug: `${node.slug}`,
          // Add optional context data to be inserted
          // as props into the page component..
          //
          // The context data can also be used as
          // arguments to the page GraphQL query.
          //
          // The page "path" is always available as a GraphQL
          // argument.
        },
      })
    })
  })
}
