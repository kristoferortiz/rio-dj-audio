import React from 'react'
import { ThemeProvider } from "styled-components"
import rioTheme from "./rio_theme"
import GlobalStyles from './globalStyles';
import './normalize.css'

const WithStyles = ({children}) => (
  <ThemeProvider theme={rioTheme}>
    <GlobalStyles />
    {children}
  </ThemeProvider>
  
)

export default WithStyles