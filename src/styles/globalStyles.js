import { createGlobalStyle } from "styled-components"

const GlobalStyles = createGlobalStyle`
  html, body, #gatsby-focus-wrapper, #___gatsby {
    height: 100%;
    font-weight: 16px;
    background-color: #000;
    font-family: ${props => props.theme.fonts.body};
  }

  h1,h2,h3 {
    font-family: ${props => props.theme.fonts.heading};
    margin: 0;
    padding: 0;
  }
  h1 {
    font-size: 1.6rem;
  }
`

export default GlobalStyles
