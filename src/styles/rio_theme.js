const headingFontFamily = [
  "Lato", 'sans-serif',
].join(',');
const bodyFontFamily = [
  "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", "Helvetica", "Arial", "Lucida Grande", "sans-serif"
].join(',');

export default {
  colors: {
    text: '#2b4b54',
    red: '#e83c44',
    orange: '#f28d34',
    yellow: '#fab737',
    green: '#27b088',
    blue: '#3498db',
    purple: '#6848ff',
    pink: '#ff6880',
    beige: '#FEFAF5',
  },
  fonts: {
    body: bodyFontFamily,
    heading: headingFontFamily,
    monospace: 'monospace',
  },
  fontSizes: [14, 16, 18, 24, 28, 36, 48],
  fontWeights: {
    body: 'normal',
    heading: 'bold',
    bold: 'bold',
  },
  lineHeights: {
    body: 1.45,
    heading: 1.1,
  },
  space: [0, 2, 4, 8, 16, 32, 64, 128, 256],
};