import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import WithStyles from "../styles"

const NotFoundPage = () => (
  <WithStyles>
    <Layout>
      <SEO title="404: Not found" />
      <h1>NOT FOUND</h1>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    </Layout>
  </WithStyles>
)

export default NotFoundPage
