import React from "react"
import WithStyles from "../styles"
import Layout from "../components/layout.jsx"
import Bienvenida from "../modules/Bienvenida"
import Socials from "../modules/Socials"
import Intro from "../modules/Intro"
import Cotizar from "../modules/Cotizar"
import Precios from "../modules/Precios"
import Video from "../modules/Video"

/* PENDIENTES */
// TODO: Fix form validation
const IndexPage = () => (
  <WithStyles>
    <Layout>
      <Intro />
      <Socials />
      <Bienvenida />
      <Video />
      <Precios />
      <Cotizar />
    </Layout>
  </WithStyles>
)

export default IndexPage
