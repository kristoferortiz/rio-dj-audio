import React from "react"
import { graphql, Link } from "gatsby"
import styled from "styled-components"
import WithStyles from "../styles"
import Layout from "../components/layout.jsx"

const StyledBlog = styled.div`
  background-color: #fff;
  flex: 1;
  padding-top: 2rem;
  h1 {
    margin: 1rem 2rem;
    @media (max-width: 650px) {
      margin: 1rem;
    }
  }
  .hasPosts {
    max-height: calc(100vh - 5.5rem);
    overflow-y: scroll;
  }
`

const StyledPost = styled.div`
  display: flex;
  height: 250px;
  margin: 2rem;
  @media (max-width: 650px) {
    flex-direction: column;
    height: 500px;
    margin: 0;
    overflow: hidden;
  }

  .hasSummary {
    display: flex;
    flex-direction: column;
    flex: 1;
    margin: 0 1em;
    @media (max-width: 650px) {
      margin: 1em;
      padding-bottom: 1em;
    }
    .continue-reading {
      background-color: ${props => props.theme.colors.orange};
      color: #fff;
      margin-top: auto;
      padding: 0.5rem;
      text-align: center;
      text-decoration: none;
      width: 8rem;
    }
    h2 {
      font-size: 1.25rem;
    }
  }
  .hasImage {
    background-size: cover;
    display: flex;
    flex: 1;
    @media (max-width: 650px) {
      min-height: 200px;
    }
  }
`

const Post = ({ post }) => {
  let excerpt = post.excerpt.substr(0, 250)
  excerpt = post.excerpt.length >= 250 ? `${excerpt} (...)` : excerpt
  return (
    <StyledPost>
      <div
        className="hasImage"
        style={{
          backgroundImage: `url(${post.featuredImage.node.sourceUrl})`,
        }}
      ></div>
      <div className="hasSummary">
        <h2>{post.title}</h2>
        <div dangerouslySetInnerHTML={{ __html: excerpt }}></div>
        <Link to={`${post.slug}`} className="continue-reading">
          {" "}
          Seguir Leyendo
        </Link>
      </div>
    </StyledPost>
  )
}

const Test = ({ data }) => {
  const {
    allWpPost: { nodes: posts },
    wp,
  } = data
  return (
    <WithStyles>
      <Layout>
        <StyledBlog>
          <h1>Nuestro Blog</h1>
          <div className="hasPosts">
            {posts.map(post => (
              <Post key={post.id} post={post} />
            ))}
          </div>
        </StyledBlog>
      </Layout>
    </WithStyles>
  )
}

export const query = graphql`
  {
    wp {
      allSettings {
        generalSettingsTitle
      }
    }
    allWpPost(filter: {categories: {nodes: {elemMatch: {name: {eq: "riodj"}}}}}) {
      nodes {
        id
        slug
        title
        excerpt

        featuredImage {
          node {
            id
            sourceUrl
          }
        }
      }
    }
  }
`

export default Test
