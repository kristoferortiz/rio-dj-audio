import React, { useState, useRef, useEffect } from "react"
import styled from "styled-components"
import Playlist from "../components/Playlist"
import { DemoContext } from "../components/DemoContext"

const Wrapper = styled.section`
  color: #fff;
  background-color: ${props => props.theme.colors.orange};
  small {
    margin: 0 1em;
  }
  .audio-container {
    position: fixed;
    left: 50%;
    top: 3em;
    margin-left: -150px;
    z-index: 999;
    &.not-visible {
      visibility: hidden;
      position: absolute;
      left: 0;
      width: 1px;
      height: 1px;
    }
    audio {
      width: 300px;
    }
  }
`
const Audio = ({ url, setCancion }) => {
  const audioRef = useRef(null)
  useEffect(() => {
    try {
      if (url) audioRef.current.play()
      audioRef.current.volume = 0.5
      audioRef.current.onended = event => {
        setCancion(currentState => false)
      }
      audioRef.current.onpause = event => {
        setCancion(currentState => false)
      }
    } catch (error) {
      console.log(error)
    }
  }, [url])
  return (
    <div className={`audio-container ${url ? '' : 'not-visible'} `}>
      <audio ref={audioRef} controls src={url}></audio>
    </div>
  )
}
const Demos = () => {
  // const audio = new Audio()
  const [cancion, setCancion] = useState(null)
  return (
    <Wrapper className="block" id="media">
      <h1>
        Demos
        {cancion && <small>Reproduciendo: {cancion.titulo}</small>}
      </h1>
      <DemoContext.Provider value={{ setCancion, cancion }}>
        <Audio
          setCancion={setCancion}
          url={cancion ? "https://gruporiopop.com.mx/" + cancion.src : ""}
        />
        <Playlist />
      </DemoContext.Provider>
    </Wrapper>
  )
}

export default Demos
