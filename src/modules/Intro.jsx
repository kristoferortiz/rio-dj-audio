import React from "react"
import styled from "styled-components"
import videoSrc from "../images/dj_rio_video.mp4" // Tell Webpack this JS file uses this image
import backgroundSrc from "../images/logo_grupo_rio_dj.svg"

const Wrapper = styled.section`
  background-color: #000;
  height: 100vmin;
  padding: 0;
  position: relative;
  width: 100%;
  .hasLogo {
    background-color: rgba(0, 0, 0, 0.5);
    background-image: url(logo_grupo_rio_dj.d79524ad.svg);
    background-position: 50%;
    background-repeat: no-repeat;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 100%;
  }
  .logo-container {
    margin-top: 1em;
    position: absolute;
    width: 100%;
    height: 100%;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: contain;
  }
`
const Intro = () => {
  return (
    <Wrapper>
      <div
        className="hasLogo"
        style={{ backgroundImage: `url(${backgroundSrc})` }}
      ></div>
      <video
        loop={true}
        muted={true}
        autoPlay={true}
        width="100%"
        height="100%"
      >
        <source src={videoSrc} type="video/mp4" />
      </video>
    </Wrapper>
  )
}

export default Intro
