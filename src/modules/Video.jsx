import React, { useEffect, useState } from "react"
import styled from "styled-components"

const Wrapper = styled.section`
  color: #fff;
  background-color: #000;
  position: relative;
  height: auto;
  h1 {
    margin: 1.5rem;
  }
  iframe {
    width: 400px;
    height: 237.5px;
    @media (max-width: 750px) {
      width: 100%;
    }
  }
`
const Video = () => {
  const [renderVideo, setRenderVideo] = useState(null)
  useEffect(() => {
    if (!renderVideo) setRenderVideo(currentState => true)
  }, [renderVideo])
  return (
    <Wrapper id="media">
      <h1> Videos </h1>
      {renderVideo && (
        <>
          <iframe
            width="560"
            height="349"
            src="//www.youtube.com/embed/ej2TIvJQDiw"
            frameBorder="0"
            title="Fiesta con Río Dj Luz y sonido Toluca"
            allowFullScreen
          ></iframe>
          <iframe
            width="560"
            height="349"
            src="//www.youtube.com/embed/Q98ZHJ31Rjo"
            frameBorder="0"
            title="Río DJ rock"
            allowFullScreen
          ></iframe>
          <iframe
            width="560"
            height="349"
            src="//www.youtube.com/embed/N1bZrW9eoAY"
            frameBorder="0"
            title="Río Dj Electronica"
            allowFullScreen
          ></iframe>
          <iframe
            width="560"
            height="349"
            src="//www.youtube.com/embed/5EMGkMfxUtU"
            frameBorder="0"
            title="Río Dj Luz y Sonido Toluca"
            allowFullScreen
          ></iframe>
        </>
      )}
    </Wrapper>
  )
}

export default Video
