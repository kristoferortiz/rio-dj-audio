import React from "react"
import styled from "styled-components"

const Wrapper = styled.section`
  color: #fff;
  background-color: ${props => props.theme.colors.purple};
`
const Bienvenida = () => {
  return (
    <Wrapper className="block" id="intro">
      <h2>Inicio</h2>
      <p>
        Río DJ es un concepto destinado para todo tipo de evento social, y sobre
        todo, que sea un éxito rotundo en cada uno de los momentos.
      </p>
      <p>
        Contamos con excelente equipo de audio, iluminación y video, actual y de
        vanguardia.
      </p>
      <p>
        No somos un simple luz y sonido, ni tenemos un simple "DJ" tocando en
        los eventos.¡Somos toda una experiencia audiovisual y divertida!
      </p>
      <p>¡No lo Dude, está con profesionales!</p>
    </Wrapper>
  )
}

export default Bienvenida
