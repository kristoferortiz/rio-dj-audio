import React, { useState } from "react"
import styled from "styled-components"
import Button from "@material-ui/core/Button"
import Forma from "../components/Forma"
import "../instagramEmbed"

const Wrapper = styled.section`
  background-color: ${props => props.theme.colors.pink};
  h1 {
    color: white;
  }
  .gracias {
    margin: 2em;
    background-color: #fff;
    max-width: 300px;
    margin: 2em auto;
    display: flex;
    justify-content: center;
    flex-direction: column;
    padding: 1em;
    h2 {
      color: #636e72;
    }
    .boton {
      margin-top: 2em;
    }
  }
`

const Cotizar = () => {
  const [wasSent, setWasSent] = useState(false)
  const onSent = () => {
    setWasSent(true)
  }
  return (
    <Wrapper className="block" id="cotizar">
      <h1>Cotizar</h1>
      {!wasSent && <Forma onSent={onSent} />}
      {wasSent && (
        <div className="gracias">
          <h2>Gracias, nos pondremos en contacto en breve!</h2>
          <Button
            className="boton"
            type="button"
            color="primary"
            onClick={() => setWasSent(currentState => false)}
          >
            Enviar otro formulario
          </Button>
        </div>
      )}
    </Wrapper>
  )
}

export default Cotizar
