import React from "react"
import styled from "styled-components"

const Wrapper = styled.section`
  color: #000;
  background-color: #fff;
  .has-overflow {
    max-width: 100%;
    max-height: 500px;
    overflow: auto;
    margin-top: 1em;
  }
  .embedsocial-instagram {
    /* width: 1200px;
    height: 800px; */
  }
`
const Galeria = () => {
  return (
    <Wrapper className="block">
      <h1>Galería</h1>
      <div className="has-overflow">
        <div
          className="embedsocial-instagram"
          data-ref="fe8f14fafd740bd5787d2ffcd2fb3b0db8a8c2f1"
        >
          Cargando...
        </div>
      </div>
    </Wrapper>
  )
}

export default Galeria
