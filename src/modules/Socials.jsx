import React from "react"
import styled from "styled-components"
import facebookLogo from "../images/logos/facebook.svg"
import youtubeLogo from "../images/logos/youtube.svg"
import twitterLogo from "../images/logos/twitter.svg"
import instagramLogo from "../images/logos/instagram.svg"

const Wrapper = styled.div`
  background-color: #000;
  display: flex;
  padding: 0.5rem;
  .social-icons {
    display: flex;
    margin: 0;
    padding: 0;
    width: 100%;
    justify-content: center;
    align-items: center;
    .social-icon {
      display: flex;
      height: 2em;
      width: 2em;
      margin: 0 0.5em;
      align-items: center;
      a,img {
        max-height: 100%;
        max-width: 100%;
        height: 2em;
        width: 2em;
      }
    }
  }
`
// TODO: Put this in config file
const socialNetworks = [
  {
    src: youtubeLogo,
    href:
      "https://www.youtube.com/channel/UCn2UCJUTpmcc_n58qrRRP1w",
  },
  { src: facebookLogo, href: "https://www.facebook.com/riodjperformance/" },
  {
    src: instagramLogo,
    href: "https://www.instagram.com/gruporiopop/?hl=es-la",
  },
  // { src: twitterLogo, href: "https://twitter.com/gruporiopop" },

]
export const SocialIcon = ({ socialNetwork }) => (
  <a href={socialNetwork.href} target="_blank" rel="noopener noreferrer">
    <img src={socialNetwork.src} alt="Facebook Logo" />
  </a>
)

const Socials = () => {
  return (
    <Wrapper>
      <ul className="social-icons">
        {
          socialNetworks.map((socialNetwork) => (
            <li key={socialNetwork.src} className="social-icon">
              <SocialIcon socialNetwork={socialNetwork} />
            </li>
          ))
        }
      </ul>
    </Wrapper>
  )
}

export default Socials
