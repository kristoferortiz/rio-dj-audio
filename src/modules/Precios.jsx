import React from "react"
import styled from "styled-components"
import Paquetes from '../components/Paquetes';

const Wrapper = styled.section`
  background-color: ${props => props.theme.colors.purple};
  color: #fff;
  .legend {
    display: block;
    text-align: center;
    font-size: 0.875em;
  }
`

const Precios = () => {
  return (
    <Wrapper className="block" id="precios">
      <h1>Paquetes</h1>
      <Paquetes />
    </Wrapper>
  )
}

export default Precios
