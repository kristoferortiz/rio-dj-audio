import axios from "axios"
import format from "date-fns/format"

export const SendForm = formData => {
  const { firstName, email, phone, evento, fecha, comentarios } = formData
  const body = {
    fields: {
      message: `Un usuario solicita cotización desde riodj.com.mx (${process.env.NODE_ENV})`,
      nombre: firstName,
      email,
      telefono: phone,
      evento,
      fecha: format(new Date(fecha), "dd-MM-yyyy"),
      comentarios: `${comentarios}`,
    },
    options: {
      from: "contactoriodj@gmail.com ",
      recipients: "contactoriodj@gmail.com ",
      subject: "Un usuario solicita cotización desde riodj.com.mx",
    },
  }
  return axios.post(process.env.GATSBY_FORM_ENDPOINT, body, {
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "x-api-key": process.env.GATSBY_FORM_API_KEY,
    },
  })
}
