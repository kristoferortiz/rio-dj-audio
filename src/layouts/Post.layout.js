import React from "react"
import { graphql } from "gatsby"
import styled from "styled-components"
import WithStyles from "../styles"
import Layout from "../components/layout.jsx"

const PostL = styled.article`
  font-family: Georgia, "Times New Roman", Times, serif;
  background-color: #fdf6e3;
  h1,
  h2 {
    font-family: Georgia, "Times New Roman", Times, serif;
  }
  p {
    font-size: 21px;
    line-height: 32px;
  }
  .head {
    width: 100%;
    height: 400px;
    position: relative;
  }

  .hasFeaturedImg {
    height: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    display: flex;
    h1 {
      bottom: 0;
      text-align: center;
      width: 100%;
      padding: 0.5em 0;
      color: #fff;
      position: absolute;
    }
  }

  .hasOverlay {
    width: 100%;
    height: 100%;
    position: absolute;
    background-color: rgba(0, 0, 0, 0.65);
  }

  .hasPost {
    margin: 2em;
    a {
      color: #268bd2;
      text-decoration: none;
      &:hover {
        text-decoration: underline;
      }
    }
  }
  img {
    max-width: 100%;
  }
`
const PostLayout = ({ data }) => {
  const { wpPost: page } = data
  return (
    <WithStyles>
      <Layout>
        <PostL>
          <div className="head">
            <div className="hasOverlay"></div>
            <div
              className="hasFeaturedImg"
              style={{
                backgroundImage: `url(${page.featuredImage.node.sourceUrl})`,
              }}
            >
              <h1>{page.title}</h1>
            </div>
          </div>
          <div
            className="hasPost"
            dangerouslySetInnerHTML={{ __html: page.content }}
          ></div>
        </PostL>
      </Layout>
    </WithStyles>
  )
}

export const query = graphql`
  query search($slug: String!) {
    wpPost(slug: { eq: $slug }) {
      excerpt
      title
      slug
      content
      desiredSlug
      seo {
        canonical
        focuskw
        metaDesc
        title
        opengraphUrl
      }
      featuredImage {
        node {
          sourceUrl
        }
      }
      date
      author {
        node {
          firstName
          lastName
        }
      }
    }
  }
`

export default PostLayout
