import React from "react"
import styled from "styled-components"
import PhoneIcon from "@material-ui/icons/Phone"
import MailOutlineIcon from "@material-ui/icons/MailOutline"
import WhatsAppIcon from "@material-ui/icons/WhatsApp"
import whatsappLogo from "../images/logos/whatsapp.svg"

const Wrapper = styled.footer`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: 0.875em;
  background-color: #2c3e50;
  color: #efefef;
  line-height: 1.5em;
  padding: 1ex 0;
  #whatsapp-container {
    display: flex;
    width: 100%;
    max-width: 800px;
    position: fixed;
    #boton-whatsapp {
      position: fixed;
      z-index: 10000;
      bottom: 1rem;
      right: 0.5rem;
      border-radius: 100%;
      color: white !important;
      padding: 12.5px;
      border: 1px solid #008a00;
      background-color: #008a00;
      box-shadow: 1px 3px 2px #3c763d;
      text-decoration: none;
      font-weight: bold;
      text-align: center;
      max-width: 75px;
      left: auto;
      display: flex;
      justify-content: center;
      margin: 1em;
      img {
        width: 2em;
      }
    }
  }
  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
  }
  a {
    color: inherit;
    text-decoration: none;
    display: flex;
    align-items: center;
    justify-content: center;
    &:hover {
      text-decoration: underline;
    }
    span {
      margin: 0.5ex 1ex;
      line-height: 1em;
    }
    svg {
    }
  }
  h1 {
    font-size: 1rem;
    line-height: 2em;
  }
  .outbound-link {
    margin: 1em;
    max-height: 40px;
    max-width: 200px;
    img {
      max-height: 40px;
      max-width: 200px;
    }
  }
`
const Pie = () => {
  return (
    <Wrapper>
      <div id="whatsapp-container">
        <div id="boton-whatsapp">
          <a
            href="https://api.whatsapp.com/send?phone=5217222270447&amp%3Btext=me+podr%C3%ADas+dar+informaci%C3%B3n+de+rio+dj+%3F&fbclid=IwAR3qVm-ovLVfwY7giUOX-oMCUArLuJu8zIJqa6ssRIHeEaCVt5j0whMCmE4"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src={whatsappLogo} alt="Whatsapp Logo" />
          </a>
        </div>
      </div>
      <h1>Información de Contacto</h1>
      <ul>
        <li>
          <a href="tel:7222270447">
            <PhoneIcon style={{ fontSize: 14 }} />
            <span>7222270447</span>
          </a>
        </li>
        <li>
          <a href="tel:7221085203">
            <PhoneIcon style={{ fontSize: 14 }} />
            <span>7221085203</span>
          </a>
        </li>
        <li>
          <a
            href="https://api.whatsapp.com/send?phone=5217222270447&amp%3Btext=me+podr%C3%ADas+dar+informaci%C3%B3n+de+rio+dj+%3F&fbclid=IwAR3qVm-ovLVfwY7giUOX-oMCUArLuJu8zIJqa6ssRIHeEaCVt5j0whMCmE4"
            target="_blank"
            rel="noopener noreferrer"
          >
            <WhatsAppIcon style={{ fontSize: 15 }} />
            <span>7222270447</span>
          </a>
        </li>
        <li>
          <a href="mailto:contactoriodj@gmail.com">
            <MailOutlineIcon style={{ fontSize: 14 }} />
            <span>contactoriodj@gmail.com</span>
          </a>
        </li>
      </ul>
      <span> Política de privacidad </span>
      <hr />
    </Wrapper>
  )
}

export default Pie
