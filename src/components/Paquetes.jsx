import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import styled from "styled-components"
import PhoneIcon from "@material-ui/icons/Phone"
import whatsappLogo from "../images/logos/whatsapp.svg"
import Paquete from "./Paquete"

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;

  #boton-whatsapp {
    display: flex;
    font-weight: bold;
    justify-content: center;
    margin: 1em;
    text-align: center;
    text-decoration: none;

    img {
      width: 3.5em;
    }
  }

  .tienePaquetes {
    display: flex;
    flex: 1;
    min-width: 100%;
    overflow: auto;

    .paquete:nth-child(1) {
      .head {
        background-color: ${props => props.theme.colors.green};
      }
    }

    .paquete:nth-child(2) {
      .head {
        background-color: ${props => props.theme.colors.orange};
      }
    }

    .paquete:nth-child(3) {
      .head {
        background-color: ${props => props.theme.colors.red};
      }
    }
  }

  .boton {
    background-color: ${props => props.theme.colors.orange};
    color: #fff;
    padding: 0.85em;
    border-radius: 100%;
    font-weight: 600;
    text-decoration: none;
    &:hover {
      background-color: ${props => props.theme.colors.red};
    }
  }

  .tieneCTA {
    align-items: center;
    display: flex;
    justify-content: center;
    min-width: 100%;
    max-height: 75px;
  }
`
const Paquetes = () => {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            paquetes {
              nombre
              feats
            }
          }
        }
      }
    `
  )
  return (
    <Wrapper>
      <div className="tienePaquetes">
        {site.siteMetadata.paquetes.map(paquete => (
          <Paquete key={paquete.nombre} paquete={paquete} />
        ))}
      </div>
      <div className="tieneCTA">
        <a
          href="tel:7222270447"
          className="boton"
          color="primary"
        >
          <PhoneIcon style={{ fontSize: 32 }} />
        </a>
        <a
          id="boton-whatsapp"
          href="https://api.whatsapp.com/send?phone=5217222270447&amp%3Btext=me+podr%C3%ADas+dar+informaci%C3%B3n+de+rio+dj+%3F&fbclid=IwAR3qVm-ovLVfwY7giUOX-oMCUArLuJu8zIJqa6ssRIHeEaCVt5j0whMCmE4"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img src={whatsappLogo} alt="Whatsapp Logo" />
        </a>
      </div>
    </Wrapper>
  )
}

export default Paquetes
