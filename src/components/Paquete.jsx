import React from "react"
import styled from "styled-components"

const Wrapper = styled.div`
  display: flex;
  flex-shrink: 0;
  width: 15em;
  flex-direction: column;
  margin: 1em 0;
  margin-right: 1em;
  .head {
    padding: 1em;
    background-color: ${props => props.theme.colors.green};
    color: #FFF;
    font-weight: 600;
    border-top-left-radius: 0.6em;
    border-top-right-radius: 0.6em;
  }
  .body {
    padding: 1em;
    background-color: #FFF;
    color: #000;
    font-size: 0.75rem;
    line-height: 1.2em;
    border-bottom-left-radius: 0.6em;
    border-bottom-right-radius: 0.6em;
    ul {
      margin-left: 1em;
      padding: 0;
      list-style-type: "- ";
      li {
        margin-bottom: 0.3em;
      }
    }
  }

`
const Paquete = ({paquete}) => {
  return (
    <Wrapper className="paquete">
      <div className="head">
        {paquete.nombre}
      </div>
      <div className="body">
        <ul>
          {
            paquete.feats.map( feat => <li key={feat}>{feat}</li>)
          }
        </ul>
      </div>
    </Wrapper>
  )
}

export default Paquete
