import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import styled from "styled-components"
import AudioElement from "./AudioElement"

const Wrapper = styled.div`
  display: flex;
 ul {
  display: flex;
  flex-wrap: wrap;
  list-style-type: none;
  @media (max-width: 650px) {
    flex-direction: column;
    margin: auto;
    padding: 1em;
  }
 }
`
const Playlist = () => {
  const { site } = useStaticQuery(
    graphql`
      query {
        site {
          siteMetadata {
            canciones {
              src
              titulo
            }
          }
        }
      }
    `
  )
  return (
    <Wrapper>
      <ul>
        {site.siteMetadata.canciones.map(cancion => {
          return (
              <AudioElement
                key={cancion.titulo}
                song={cancion}
              />
          )
        })}
      </ul>
    </Wrapper>
  )
}

export default Playlist
