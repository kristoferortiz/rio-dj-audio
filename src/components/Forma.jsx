import React, { useState } from "react"
import DateFnsUtils from "@date-io/date-fns" // choose your lib
import { navigate } from "gatsby"
import TextField from "@material-ui/core/TextField"
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
  DatePicker
} from "@material-ui/pickers"
import MenuItem from "@material-ui/core/MenuItem"
import FormHelperText from "@material-ui/core/FormHelperText"
import CircularProgress from "@material-ui/core/CircularProgress"
import FormGroup from "@material-ui/core/FormGroup"
import Button from "@material-ui/core/Button"

import Select from "@material-ui/core/Select"
import styled from "styled-components"

import { useFormik } from "formik"
import * as Yup from "yup"
import { SendForm } from "../services/formSubmission"

const helperTexts = {
  email: {
    required: "Ingresa el email de quien solicita información",
    email: "Se necesita un email válido (email@dominio.com)",
  },
  firstName: {
    min: "Son mínimo 5 letra",
    required: "Ingresa el nombre de quien solicita información",
  },
  phone: {
    required: "Ingresa el teléfono de quien solicita información",
  },
  evento: {
    required: "Selecciona una evento",
  },
  fecha: {
    required: "Selecciona una fecha",
  },
}
const Wrapper = styled.form`
  margin: 2em;
  background-color: #fff;
  max-width: 300px;
  margin: 2em auto;
  display: flex;
  justify-content: center;
  flex-direction: column;
  padding: 1em;
  .privacy-notice {
    font-size: 0.75em;
    margin: 1em 0;
    display: block;
    color: #000;
    line-height: 1.3em;
    font-weight: 600;
  }
  p {
    margin: 0;
  }
  .datepicks {
    margin-top: 0;
  }
  .hCaptcha {
    margin-top: 1em;
  }
  .selector-evento {
    margin-top: 1em;
  }
`
const SignupForm = ({ onSent }) => {
  // TODO: Prevent scrolling when selecting date
  // TODO: Prevent form from staying invalid until click
  // TODO: Parsear fechas
  const [selectedDate, setSelectedDate] = useState(null)
  const [isOpen, setIsOpen] = useState(false)
  const [isSending, setIsSending] = useState(false)

  const formik = useFormik({
    initialValues: {
      firstName: "",
      email: "",
      phone: "",
      evento: "",
      fecha: "",
      comentarios: "",
    },
    validationSchema: Yup.object({
      firstName: Yup.string()
        .min(3, helperTexts.firstName.min)
        .required(helperTexts.firstName.required),
      email: Yup.string()
        .email(helperTexts.email.email)
        .required(helperTexts.email.required),
      phone: Yup.string().required(helperTexts.phone.required),
      evento: Yup.string().required("Selecciona un evento"),
      fecha: Yup.string().required("Selecciona una fecha"),
    }),
    onSubmit: async values => {
      setIsSending(currentState => true)
      await SendForm(values)
      setIsSending(currentState => false)
      onSent()
    },
  })

  const handleDateChange = newDate => {
    if (newDate) {
      setSelectedDate(newDate)
      setIsOpen(false)
      formik.values.fecha = newDate
      formik.validateForm();
    }
  }

  return (
    <Wrapper onSubmit={formik.handleSubmit} noValidate autoComplete="on">
      <p>Puedes solicitar una cotización llenando el siguiente formulario.</p>
      <FormGroup>
        <TextField
          required
          type="text"
          label="Nombre"
          id="firstName"
          name="firstName"
          {...formik.getFieldProps("firstName")}
          error={Boolean(formik.touched.firstName && formik.errors.firstName)}
          helperText={formik.errors.firstName || helperTexts.firstName.required}
        />

        <TextField
          required
          type="email"
          label="Email"
          id="email"
          name="email"
          {...formik.getFieldProps("email")}
          error={Boolean(formik.touched.email && formik.errors.email)}
          helperText={formik.errors.email || helperTexts.email.required}
        />

        <TextField
          required
          type="text"
          label="Teléfono"
          id="phone"
          name="phone"
          {...formik.getFieldProps("phone")}
          error={Boolean(formik.touched.phone && formik.errors.phone)}
          helperText={formik.errors.phone || helperTexts.phone.required}
        />
        <Select
          required
          className="selector-evento"
          id="evento"
          name="evento"
          displayEmpty
          {...formik.getFieldProps("evento")}
        >
          <MenuItem value="" disabled selected>
            Evento *
          </MenuItem>
          <MenuItem value="boda">Boda</MenuItem>
          <MenuItem value="XV">XV Años</MenuItem>
          <MenuItem value="fin de año">Fin de Año</MenuItem>
          <MenuItem value="graduación">Graduación</MenuItem>
          <MenuItem value="corporativo">Evento Corporativo</MenuItem>
          <MenuItem value="otro">Otro tipo</MenuItem>
        </Select>
        <FormHelperText
          error={Boolean(formik.touched.evento && formik.errors.evento)}
        >
          {formik.errors.evento
            ? formik.errors.evento
            : "Tipo de evento que se celebrará"}
        </FormHelperText>
        <div>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <DatePicker
              className="datepicks"
              disableToolbar
              variant="inline"
              format="dd/MM/yyyy"
              margin="normal"
              id="date-picker-inline"
              label="Fecha"
              KeyboardButtonProps={{
                "aria-label": "change date",
              }}
              open={isOpen}
              onOpen={() => setIsOpen(true)}
              onClose={() => setIsOpen(false)}
              value={selectedDate}
              onChange={handleDateChange}
            />
          </MuiPickersUtilsProvider>
          <FormHelperText
            error={Boolean(formik.touched.fecha && formik.errors.fecha)}
          >
            {formik.errors.fecha
              ? formik.errors.fecha
              : "Fecha en la que se celebraría tu evento"}
          </FormHelperText>
        </div>
        <TextField
          id="comentarios"
          name="comentarios"
          placeholder="Algo más que nos quieras decir ?"
          multiline
          rows={2}
          rowsMax={4}
          label="Comentarios"
          helperText="Información extra que quieras que sepamos"
          {...formik.getFieldProps("comentarios")}
        />
        {isSending && <CircularProgress color="secondary" />}
        {!isSending && (
          <div>
            {!formik.isValid && (
              <FormHelperText error>
                <br />
                Completa correctamente todos los campos requeridos (tienen un
                asterisco)
              </FormHelperText>
            )}
            <div>
              <span className="privacy-notice">
                Al hacer click en enviar aceptas la política de privacidad *
              </span>
              <Button type="submit" variant="contained" color="primary">
                Enviar
              </Button>
            </div>
          </div>
        )}
      </FormGroup>
    </Wrapper>
  )
}

export default SignupForm
