import React, { useContext } from "react"
import styled from 'styled-components';
import { DemoContext } from './DemoContext';
import PlayIcon from '@material-ui/icons/PlayCircleOutline';
import PauseIcon from '@material-ui/icons/PauseCircleFilled';

const Wrapper = styled.li`
  cursor: pointer;
  flex: 1 1 auto;
  cursor: pointer;
  display: flex;
  align-items: center;
  span {
    line-height: 1.5em;
    margin-left: 0.5rem;
  }
  @media (min-width: 650px) {
    width: 50%;
  }
`
const AudioElement = ({song}) => {
  const {cancion, setCancion} = useContext(DemoContext)

  const handleClick = () => {
    if ( cancion && cancion.src === song.src ) {
      return setCancion( (currentState) => null )
    }
    setCancion( (currentState) => song )
  }
  
  return (
    <Wrapper onClick={handleClick} >
      {
        (cancion && cancion.src === song.src) && <PauseIcon style={{ fontSize: 20 }}/>
      }
      {
        (!cancion || cancion.src !== song.src) && <PlayIcon style={{ fontSize: 20 }}/>
      }
      <span>
        {song.titulo}
      </span>
    </Wrapper>
  )
};

export default AudioElement;