import React from 'react'
import styled from 'styled-components'

const Section2 = ({id = 'a' + Math.random()}) => {
  const Section = styled.section`
    min-height: 800px;
  `
  return (
    <Section id={id}>
      Section 2
    </Section>
  )
}

export default Section2