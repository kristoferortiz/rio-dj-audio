import React from "react"
import { Link } from "gatsby"
import { AnchorLink } from "gatsby-plugin-anchor-links"
import styled from "styled-components"

const StyledNavigation = styled.nav`
  display: flex;
  width: 100%;
  max-width: 800px;
  position: fixed;

  box-shadow: 0px 2px 2px 0px rgba(0, 0, 0, .2);
  height: 2.1em;
  background-color: rgba(255, 255, 255, .8);
  z-index: 999;
  top: 0;
  ul {
    display: flex;
    flex: 1;
    list-style-type: none;
    margin: 0;
    padding: 0;
    justify-content: center;
    li {
      font-family: ${props => props.theme.fonts.heading};
      width: 25%;
      max-width: 150px;
      font-weight: 300;
      line-height: 1.5em;
      text-transform: lowercase;
      text-align: center;
      &:nth-child(1) {
        border-top: 6px solid ${props => props.theme.colors.red};
        a {
          &:hover {
            background-color: ${props => props.theme.colors.red};
          }
        }
      }
      &:nth-child(2) {
        border-top: 6px solid #f28d34;
        a {
          &:hover {
            background-color: #f28d34;
          }
        }
      }
      &:nth-child(3) {
        border-top: 6px solid #fab737;
        a {
          &:hover {
            background-color: #fab737;
          }
        }
      }
      &:nth-child(4) {
        border-top: 6px solid #000000;
        a {
          &:hover {
            background-color: #000000;
          }
        }
      }
      &:nth-child(5) {
        border-top: 6px solid #000000;
        a {
          &:hover {
            background-color: #000000;
          }
        }
      }
      a {
        display: block;
        text-decoration: none;
        color: #666;
        text-decoration: none;
        padding-bottom: 0.25rem;
        &:hover {
          color: #FFF;
        }
        
      }
    }
  }
`

const Nav = () => {
  return (
    <StyledNavigation>
      <ul>
        <li>
          <AnchorLink
            to="/#intro"
            title="Inicio"
            className="stripped"
            stripHash={false}
          />
        </li>
        <li>
          <AnchorLink
            to="/#precios"
            title="Paquetes"
            className="stripped"
            stripHash={false}
          />
        </li>
        <li>
          <AnchorLink
            to="/#cotizar"
            title="Cotizar"
            className="stripped"
            stripHash={false}
          />
        </li>
        <li>
          <AnchorLink
            to="/blog"
            title="Blog"
            className="stripped"
            stripHash={false}
          />
        </li>
      </ul>
    </StyledNavigation>
  )
}

export default Nav
