import React from "react"
import styled from "styled-components"
import Nav from "./nav"
import Pie from "./pie"
import SEO from "./seo"

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  min-height: 100%;
  position: relative;
  background: aquamarine;
  width: 100%;
  max-width: 800px;
  @media (min-width: 800px) {
    margin: 0 auto 1rem auto;
    box-shadow: 0px 0px 25px 1px rgba(0, 0, 0, 0.6);
  }
  .block {
    padding: 1.5em;
    p {
      font-weight: 300;
    }
  }
`
const Layout = ({ children } = {}) => {
  return (
    <>
      <SEO />
      <Wrapper>
        <Nav />
        {children}
        <Pie />
      </Wrapper>
    </>
  )
}

export default Layout
